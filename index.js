var express = require('express')
var mongoose = require('mongoose')
var app = express()

// Middleware
var bodyParser = require('body-parser')
var logger = require('morgan')
var errorHandler = require('errorhandler')

// Setup mongoose connection, Post model, postSchema, posts colleciton 
//var dbUri = 'mongodb://appuser:apppass@dogen.mongohq.com:10054/tsims-product'
var dbUri = 'mongodb://localhost:27017/app'
var dbConnection = mongoose.createConnection(dbUri);
var Schema = mongoose.Schema
var postSchema = new Schema({
    title: {
        type: String,
        required: true,
        trim: true,
        match: /^([\w ,.!?]{1,100})$/
    },
    text: {
        type: String,
        required: true,
        max: 2000
    },
    followers: [Schema.Types.ObjectId],
    meta: Schema.Types.Mixed,
    comments: [{
        text: {
            type: String,
            trim: true,
            max: 1000,
        },
        author: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        name: String
    }],
    viewCounter: Number,
    published: Boolean,
    createdAt: {
        type: Date,
        default: Date.now,
        required: true
    },
    updatedAt: {
        type: Date,
        default: Date.now,
        reuqired: true
    }
})
var Post = dbConnection.model('Post', postSchema, 'posts') // Model, Schema, Collection

// Use Middleware in all routes
app.use(express.static('client')) // Connect to Client
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
        extended: true
    })) // extended: true > nested object can be parsed 

/* REST Starts */

// Route GET
app.get('/api', function(req, res) {
    res.send('ok')
})

app.get('/api/posts', function(req, res, next) {
    Post.find({}, function(error, posts) {
        if (error) return next(error)
        res.send(posts)
    })
})

app.get('/api/posts/:id', function(req, res, next) {
    Post.findOne({_id: req.params.id}, function(error, post) {
        if (error) return next(error)
        res.send(post.toJSON())
    })
})

//Route POST and create mongoose's save method with Middleware bodyParser (req.body)
app.post('/api/posts', function(req, res, next) {
    var post = new Post(req.body)
    post.validate(function(error) {
        console.log(error)
        if (error) return next(error)
        post.save(function(error, results) {
            if (error) return next(error)
            res.send(results)
        })
    })
})

//Route PUT
app.put('/api/posts/:id', function(req, res, next) {
    Post.findOne({_id: req.params.id}, function(error, post) {
        if (error) return next(error)
        post.set(req.body)
        post.save(function(error, results) {
            if (error) return next(error)
            res.send(results.toJSON())
        })
    })
})

app.delete('/api/posts/:id', function(req, res, next) {
  Post.findOne({_id: req.params.id}, function(error, post){
    if (error) return next(error)
    post.remove(function(error, results){
        if (error) return next(error)
        res.send(results)
    })
  })
})

// Use error handler middleware
app.use(errorHandler())

var server = require('http').createServer(app).listen(5000)
