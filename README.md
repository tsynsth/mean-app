# README #

![App Preview](http://tuningsynesthesia.com/_corporate/app/app.png "App Preview")

### Summary ###

A simple MEAN CRUD Post application with basic but thorough REST API. The API was initially built along Udemy Course "[Mongoose: Master Object-Document Mapper for Node.js and Express.js](https://www.udemy.com/mongoose/)" by Azat Mardan which was solely for introducing how to build API using Mongoose for CRUD operation of "Posts". The API was extended and client side codes using Angular.js were added by the author in this version of app. It is one of the simplest examples of a complete MEAN application. For the version only with API, please see "[Bitbucket: Mongoose API](https://bitbucket.org/cfuyuki/mongoose-api)".

### Prerequisites ###

You need to have those before installation:

* Node.js
* MongoDB
* Internet Connection

### Installation ###

1: Clone or download this repository.

2: Install the Node.js dependencies:

```
#!javascript

$ npm install
```

3: Run the app:

```
#!javascript

$ node index.js
```

### Dependencies ###

* [Mongoose](http://mongoosejs.com/)
* [Express 4.x](http://expressjs.com/)
* [Express bodyParser](https://github.com/expressjs/body-parser)
* [Express Morgan](https://github.com/expressjs/morgan)


* [AngularJS](https://angularjs.org/)
* AngularJS ngResource
* AngularJS ngRoute
* [Bootstrap](http://getbootstrap.com/)
* [UI Bootstrap](https://angular-ui.github.io/bootstrap/)

### Database ###

This application uses the next MongoDB configuration. You may change any of those in `index.js`.

* Database name: app
* Collection name: posts 

To bulk-import sample data from "sampleData.json" to local MongoDB installation,
in CLI, change directory to the app folder and run:

```
mongoimport -d app -c posts --jsonArray < sampleData.json
```